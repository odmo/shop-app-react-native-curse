import React, { useState } from 'react';
import { useFonts } from '@use-expo/font';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import { enableScreens, useScreens } from 'react-native-screens';
import productsReducer from './store/reducers/products';
import cartReducer from './store/reducers/cart';
import ordersReducer from './store/reducers/orders';
import ShopNavigator from './navigation/ShopNavigator';
import { AppLoading } from 'expo';
import authReducer from './store/reducers/auth';
import NavigationContainer from './navigation/NavigationContainer';
//import * as Font from 'expo-font'; 
//import { composeWithDevTools } from 'redux-devtools-extension';

enableScreens();

const rootReducer  = combineReducers({

  products : productsReducer,
  cart : cartReducer,
  orders : ordersReducer,
  auth : authReducer 
});
// Para debuggearlo con react native debugger 
const store = createStore( rootReducer, applyMiddleware( ReduxThunk ) ); //, composeWithDevTools() );

export default function App() {

  let [fontsLoaded] = useFonts({
    'open-sans' : require ('./assets/fonts/OpenSans-Regular.ttf'), 
    'open-sans-bold' : require('./assets/fonts/OpenSans-Bold.ttf')
  });

  if (!fontsLoaded){
    return (
      <AppLoading />
    );
  }else{
    return (
      <Provider store = { store } >
        <NavigationContainer/>
      </Provider>
  );
  }
}