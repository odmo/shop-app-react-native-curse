import React from 'react';
import { Platform, View, SafeAreaView, Button } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator, DrawerItems } from 'react-navigation-drawer';
import ProductsOverviewScreen from '../screen/shop/ProductsOverviewScreen';
import ProductDetailScreen from '../screen/shop/ProductDetailScreen';
import OrdersScreen from '../screen/shop/OrdersScreen';
import EditProductScreen from  '../screen/user/EditProductScreen';
import CartScreen from '../screen/shop/CartScreen';
import Colors from '../constant/Colors';
import UserProductsScreen from '../screen/user/UserProductsScreen';
import CustomHeaderButton from '../components/UI/HeaderButton';
import AuthScreen from '../screen/user/AuthScreen';
import { Ionicons } from '@expo/vector-icons';
import StartupScreen from '../screen/StartupScreen';
import { useDispatch } from 'react-redux';
import * as authActions from '../store/actions/auth';

const defaultNavOptions = {
    headerStyle : {
        backgroundColor : ( Platform.OS === 'android' ) ? Colors.primary : '',
    },
    headerTitleStyle : {
        
    },
    headerBackTitleStyle : {
       
    },
    headerTintColor : ( Platform.OS === 'android' ) ? 'white' : Colors.primary
};


const ProductsNavigator = createStackNavigator(
    {
        ProductsOverview : ProductsOverviewScreen,
        ProductDetail : ProductDetailScreen,
        Cart : CartScreen
    },
    {
        navigationOptions: {
            drawerLabel: 'Cart',
            drawerIcon: <CustomHeaderButton
            text = 'Cart'
            onSelect = { () => navData.navigation.toggleDrawer() }
            color = { (Platform.OS === 'android') ? Colors.accent : Colors.primary }
            />
            // drawerIcon: drawerConfig  => <Ionicons  
            //     name = {(Platform.OS === 'android') ? 'md-cart' : 'ios-cart' }
            //     size = {23}
            //     color = { drawerConfig.tintColor }

            // />
        },
        defaultNavigationOptions :  defaultNavOptions 
    }
);

const OrdersNavigator = createStackNavigator(
    {
        Orders: OrdersScreen
    },
    {
        navigationOptions: {
            drawerLabel: 'Order',
            drawerIcon: <CustomHeaderButton
            text = 'Order'
            onSelect = { () => navData.navigation.toggleDrawer() }
            color = { (Platform.OS === 'android') ? Colors.accent : Colors.primary }
            />
            // drawerIcon: drawerConfig  => <Ionicons  
            //     name = {(Platform.OS === 'android') ? 'md-list' : 'ios-list' }
            //     size = {23}
            //     color = { drawerConfig.tintColor }

            // />
        },
        defaultNavigationOptions :  defaultNavOptions 
    } 
);

const AdminNavigator = createStackNavigator(
    {
        UserProducts : UserProductsScreen,
        EditProduct : EditProductScreen
    },
    {
        navigationOptions: {
            drawerLabel: 'Admin',
            drawerIcon: <CustomHeaderButton
                text = 'Admin'
                onSelect = { () => navData.navigation.toggleDrawer() }
                color = { (Platform.OS === 'android') ? Colors.accent : Colors.primary }
            />
            // drawerIcon: drawerConfig  => <Ionicons  
            //     name = {(Platform.OS === 'android') ? 'md-list' : 'ios-list' }
            //     size = {23}
            //     color = { drawerConfig.tintColor }

            // />
        },
        defaultNavigationOptions :  defaultNavOptions 
    } 
);

const ShopNavigator = createDrawerNavigator(
    {
        Products : ProductsNavigator,
        Orders : OrdersNavigator,   
        Admin : AdminNavigator 
    },
    {
        contentOptions : {
            activeTintColor : Colors.primary
        },
        contentComponent : props => {
            const dispatch = useDispatch();
            return (
                <View style = {{ flex : 1, paddingTop : 20 }}>
                    <SafeAreaView forceInset = { { top : 'always', horizontal : 'never' } } >
                        <DrawerItems 
                            { ...props }
                        />
                        <Button title = "Logout" color = { Colors.primary } onPress = { () => {
                            dispatch(authActions.logout( ));
                            props.navigation.navigate('Auth');
                        } } />
                    </SafeAreaView>
                </View>
            );

        }
    } 
);

const AuthNavigation = createStackNavigator(
    {
        Auth : AuthScreen  
    },{
        defaultNavigationOptions : defaultNavOptions
    }
);

const MainNavigator = createSwitchNavigator (
    {
        Startup : StartupScreen,
        Auth : AuthNavigation,
        Shop : ShopNavigator
    }
);

export default createAppContainer( MainNavigator );