import React from 'react';
import {  FlatList, Platform, Button, Alert, View, Text } from 'react-native';
import { useSelector, useDispatch } from 'react-redux' 
import ProductItem from '../../components/shop/ProductItem'
import CustomHeaderButton from '../../components/UI/HeaderButton';
import Colors from '../../constant/Colors';
import * as productsActions from '../../store/actions/products';

const UserProductScreen = props => {
    const userProducts = useSelector( state => state.products.userProducts );
    
    const distpatch = useDispatch();

    const editProductHandler = (id) => {
        props.navigation.navigate('EditProduct', {productId: id});
    };

    
    const deleteHandler = (id) => {
        Alert.alert('Are you sure ?' , 'Do you really whant to delete this item?', 
        [
            { text : 'No', style : 'default' },
            { text : 'Yes', style : 'destructive', onPress: () => { 
                distpatch(productsActions.deleteProduct(id))
            }}
        ]);
    }

    if ( userProducts.length === 0 ){
        return (
            <View style = { { flex : 1, justifyContent : 'center', alignItems : 'center' } } > 
                <Text> No products found, maybe start creating some ? </Text>
            </View>
        );
    }

    return (
        <FlatList
            data = { userProducts }
            keyExtractor = { item => item.id }
            renderItem = { itemData => <ProductItem 
                    image = { itemData.item.imageUrl }
                    title = { itemData.item.title }
                    price = { itemData.item.price }
                    onSelect = {() => {}}
                > 
                    <Button 
                        title = "Edit" 
                        onPress = { () => {
                            editProductHandler(itemData.item.id);
                        } } 
                        color = { Colors.primary }
                    />
                    <Button 
                        title = "Delete"  
                        onPress = { deleteHandler.bind( this, itemData.item.id )  } 
                        color = { Colors.primary }
                    />
                </ProductItem>
            }
        />
    );
};

UserProductScreen.navigationOptions = navData =>  {
    return { 
        headerTitle : 'Your Products',
        headerLeft : () => (
            <CustomHeaderButton
                text = 'Menu'
                onSelect = { () => navData.navigation.toggleDrawer() }
                color = { (Platform.OS === 'android') ? Colors.accent : Colors.primary }
            />
        ),
        headerRight : () => (
            <CustomHeaderButton
                text = 'Add'
                onSelect = { () => navData.navigation.navigate('EditProduct') }
                color = { (Platform.OS === 'android') ? Colors.accent : Colors.primary }
            />
        )
    
    }
}


export default UserProductScreen;