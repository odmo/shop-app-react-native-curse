import React, { useEffect, useCallback, useReducer, useState  } from 'react';
import { View, ScrollView, StyleSheet, Platform, Alert, KeyboardAvoidingView, ActivityIndicator } from 'react-native';
import { useSelector, useDispatch } from 'react-redux' 
import ProductItem from '../../components/shop/ProductItem'
import CustomHeaderButton from '../../components/UI/HeaderButton';
import Colors from '../../constant/Colors';
import * as productsActions from '../../store/actions/products';
import Input from '../../components/UI/Input';

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';
 
const formReducer = (state, action ) => {
    if (action.type === FORM_INPUT_UPDATE){
        
        const updatedValues = {
            ...state.inputValues,
            [ action.input ]: action.value
        };

        const updatedValidities = { 
            ...state.inputValidities,
            [ action.input ]: action.isValid
        };

        let updatedFormIsValid = true;
        for ( const key in updatedValidities ){
            updatedFormIsValid = updatedFormIsValid && updatedValidities[key];
        }

        return {
            formIsValid : updatedFormIsValid,
            inputValidities : updatedValidities,
            inputValues : updatedValues 
        };
    }
    return state;
};
 
const EditProductScreen = props => {

    const [ isLoading, setIsloading ] = useState(false);
    const [ error, setError ] = useState(); // valor inicial == null

    const prodId = props.navigation.getParam('productId');
    const editedProduct= useSelector ( state => state.products.userProducts.find( prod => prod.id === prodId ) );

    const distpatch = useDispatch();

    const [formsState, distpatchFormState ] = useReducer(formReducer, { 
        inputValues: {
            title: editedProduct ? editedProduct.title : '' ,
            imageUrl : editedProduct ? editedProduct.imageUrl : '',
            description : editedProduct ? editedProduct.description : '',
            price: ''
        }, 
        inputValidities: {
            title : editedProduct ? true : false,
            imageUrl : editedProduct ? true : false, 
            description : editedProduct ? true : false, 
            price : editedProduct ? true : false, 
        }, formIsValid : editedProduct ? true : false  
    })

    const [ title, setTitle ] = useState(editedProduct ? editedProduct.title : '');
    const [ titleIsValid, setTitleIsValid ] = useState (false);
    const [ imageUrl, setImageUrl] = useState(editedProduct ? editedProduct.imageUrl : '');
    const [ imageUrlIsValid, setImageUrlIsValid ] = useState(false);
    const [ price, setPrice ] = useState('');
    const [ priceIsValid, setPriceIsValid ] = useState(false);
    const [ description, setDescription] = useState(editedProduct ? editedProduct.description : '');
    const [ descriptionIsValid, setDescriptionIsValid ] = useState(false);

    useEffect ( () => {
        if (error){
            Alert.alert('An error occurred!', error, [{ text : 'Okay' }]);
        }
    },  [error] )

    const submitHandler = useCallback( async () => {
        if ( !formsState.formIsValid ){
            Alert.alert('Wrong input', 'Please check the errors in the form', [ { text : 'Okay'} ]  )
            return;
        }

        setError(null); 
        setIsloading(true);
        try 
        {
            if (editedProduct){
                await distpatch( productsActions.updateProduct( prodId, formsState.inputValues.title, formsState.inputValues.description, formsState.inputValues.imageUrl ) );
            }else {
                await  distpatch( productsActions.createProduct( formsState.inputValues.title, formsState.inputValues.description, formsState.inputValues.imageUrl, +formsState.inputValues.price ) );
            }    
            props.navigation.goBack();  
        }catch (err)
        {
            setError(err.message);
        }
        

        setIsloading(false);
         
    }, [ distpatch, prodId, formsState ] );

    useEffect(() =>{
        props.navigation.setParams({submit : submitHandler });
    }, [submitHandler]);

    const inputChangeHandler = useCallback(
        (inputIdentifier, inputValue, inputValidity) => { 
        distpatchFormState({
            type: FORM_INPUT_UPDATE,
            value: inputValue,
            isValid: inputValidity,
            input: inputIdentifier
          });
        },
        [distpatchFormState]
    );

    if (isLoading) {
        return  <View style = { styles.centered } ><ActivityIndicator size = 'large' color = { Colors.primary } /></View>
    }
    return (
        <KeyboardAvoidingView  style = { { flex : 1 } } behavior = "padding" keyboardVerticalOffset = {100} >
            <ScrollView>
                <View style = { styles.form }>
                    <Input 
                        id = 'title'
                        label = 'Title '
                        errorText = 'Please enter a valid title!'
                        keyboardType = 'default'
                        autoCapitalize = 'sentences'
                        autoCorrect
                        returnKeyType = 'next'
                        onInputChange = { inputChangeHandler }
                        initialValue = { editedProduct ? editedProduct.title : '' }
                        initiallyValid = { !!editedProduct }
                        required

                    />
                    <Input 
                        id = 'imageUrl'
                        label = 'ImageUrl'
                        errorText = 'Please enter a valid imageUrl!'
                        keyboardType = 'default'
                        autoCapitalize = 'sentences'
                        autoCorrect
                        returnKeyType = 'next'
                        onInputChange = { inputChangeHandler }
                        initialValue = { editedProduct ? editedProduct.imageUrl : '' }
                        initiallyValid = { !!editedProduct }
                        required
                    />
                    {editedProduct ? null : (
                        <Input 
                            id = 'price'
                            label = 'Price '
                            errorText = 'Please enter a valid price!'
                            keyboardType = 'decimal-pad'
                            returnKeyType = 'next'
                            onInputChange = { inputChangeHandler }
                            required
                            min = { 0.1 }
                        />
                    )}
                    <Input 
                        id = 'description'
                        label = 'Description '
                        errorText = 'Please enter a valid description!'
                        keyboardType = 'default'
                        autoCapitalize = 'sentences'
                        autoCorrect
                        multiline
                        numerOfLines = {3}
                        onInputChange = { inputChangeHandler }
                        initialValue = { editedProduct ? editedProduct.description : '' }
                        initiallyValid = { !!editedProduct }
                        required
                        minLength = { 5 }
                    />
                </View>
            </ScrollView>
        </KeyboardAvoidingView>
    );
};

EditProductScreen.navigationOptions = navData =>{
    const submitFn = navData.navigation.getParam('submit');

    return {
        headerTitle : navData.navigation.getParam('productId')
        ? 'Edit Product'
        : 'Add Product',
        headerRight : () => (
            <CustomHeaderButton
                text = 'Save'
                onSelect = { submitFn }
                color = { (Platform.OS === 'android') ? Colors.accent : Colors.primary }
            />
        )
    }
};

const styles = StyleSheet.create({
    form : {
        margin : 20,
    },
    centered : {
        flex : 1 , 
        justifyContent : 'center',
        alignItems : 'center'
    }
});

export default EditProductScreen;