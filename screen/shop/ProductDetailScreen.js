import React from 'react';
import { ScrollView, Text, Image, Button, StyleSheet, View } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import Colors from '../../constant/Colors';
import * as CartActions from '../../store/actions/cart';

const ProductDetailScreen = props => {

    const productId = props.navigation.getParam('productId');
    const selectedProduct = useSelector( state => state.products.availableProducts.find( prod => prod.id === productId ) );

    const distpatch = useDispatch();

    return(
        <ScrollView>
            <Image
                style = { styles.image }
                source = {{ uri : selectedProduct.imageUrl }}
            />
            <View style = { styles.actions } >
                <Button
                    color = { Colors.primary }
                    title = " Add to Cart " 
                    onPress = { () => {
                        distpatch( CartActions.addToCart( selectedProduct ) )
                    } }
                />
            </View>
            <Text style = { styles.price } > $ { (selectedProduct.price != null ) ? selectedProduct.price.toFixed(2) : 0 } </Text>
            <Text style = { styles.description }  > { selectedProduct.description } </Text>
        </ScrollView>
    );
};

ProductDetailScreen.navigationOptions = navData => {
    
    return (
        {
            headerTitle : navData.navigation.getParam('productTitle'), 
        }
    );

};


const styles = StyleSheet.create({
    image : {
        width : '100%',
        height : 300
    },
    price : {
        fontSize : 20,
        color : '#888',
        textAlign : 'center',
        marginVertical : 20,
        
    },
    description : {
        fontSize : 14,
        textAlign : 'center',
        marginHorizontal : 20    
    },
    actions : {
        alignItems : 'center',
        marginVertical : 20
    }
});

export default ProductDetailScreen;