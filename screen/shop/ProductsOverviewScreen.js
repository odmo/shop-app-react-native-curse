import React, { useState, useEffect, useCallback } from 'react';
import { FlatList, StyleSheet, Button, ActivityIndicator, View, Text } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import ProductItem from '../../components/shop/ProductItem';
import * as CartActions from '../../store/actions/cart';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import CustomHeaderButton from '../../components/UI/HeaderButton';
import * as productActions from '../../store/actions/products';
import Colors from '../../constant/Colors';

const ProductsOverviewScreen  = props => {
    const [ isLoading, setIsLoading ] = useState( false );
    const [ isRefreshing, setIsRefreshing ] = useState ( false )
    const [ error, setError ] = useState();
    const products = useSelector( state => state.products.availableProducts );  
    const distpatch = useDispatch();


    const loadProducts = useCallback( (async () => {
        setError(null);
        setIsRefreshing ( true );
        try
        {
            await distpatch( productActions.fetchProducts() );
        }catch(err){
            setError( err.message );
        }
        setIsRefreshing( false )
    }), [ distpatch, setIsLoading, setError ]);

    useEffect ( () => {
        const willFoucs = props.navigation.addListener( 'willFoucs', loadProducts ); 
        return () => {
            willFoucs.remove();
        };
    }, [ loadProducts ] );

    useEffect ( () => {
        setIsLoading( true );
        loadProducts().then( () => {
            setIsLoading(false);
        }) ;
    }, [ distpatch, loadProducts ] )

    const selectItemHandler = (id, title) => {
        props.navigation.navigate(
            'ProductDetail', 
            { 
                productId : id,
                productTitle : title 
            }
        );
    };

    if (isLoading){
        return(
            <View style={ styles.centered }>
                <ActivityIndicator size= "large" color = { Colors.primary } />
            </View>
        );
    }

    if ( error ){
        return(
            <View style={ styles.centered }>
                <Text> An error occurred! </Text>
            </View>
        );
    }

    if (!isLoading && products.length === 0 ){
        return(
            <View style={ styles.centered }>
                <Text size= "large" color = { Colors.primary } > No products found. Maybe start adding some ! </Text>
                <Button 
                    title = 'Try again' 
                    onPress = { loadProducts }
                    color = { Colors.primary }
                />
            </View>
        );
    }
 
    return (
        <FlatList
            onRefresh = { loadProducts }
            refreshing = { isLoading }
            data = { products } 
            keyExtractor = { item => item.id  } 
            renderItem = { 
                itemData => 
                    <ProductItem
                        title = { itemData.item.title }
                        price = { itemData.item.price }
                        image = { itemData.item.imageUrl }
                        onSelect = { () => { 
                            selectItemHandler ( itemData.item.id, itemData.item.title )
                        } }
                        onAddToCart = { () => {
                            distpatch( CartActions.addToCart( itemData.item ) );
                        } }
                    >
                        <Button 
                            title = "View Details" 
                            onPress = { () => { 
                                selectItemHandler ( itemData.item.id, itemData.item.title )
                            } } 
                            color = { Colors.primary }
                        />
                        <Button 
                            title = "To Cart"  
                            onPress = { () => {
                                distpatch( CartActions.addToCart( itemData.item ) );
                            } } 
                            color = { Colors.primary }
                        />
                    </ProductItem>
            }
        />
    );
};

ProductsOverviewScreen.navigationOptions = navData => {


    return {

        headerTitle : 'All Products',
        headerLeft : () => (
            <CustomHeaderButton
                text = 'Orders'
                onSelect = { () => navData.navigation.toggleDrawer() }
                color = { (Platform.OS === 'android') ? Colors.accent : Colors.primary }
            />
        ),
        headerRight: () => (
            <CustomHeaderButton
                text = 'cart'
                onSelect = { () => navData.navigation.navigate('Cart') }
                color = { (Platform.OS === 'android') ? Colors.accent : Colors.primary }
            />
        ),
        // headerRigth : () => ( <HeaderButtons HeaderButtonComponent = { CustomHeaderButton } >
        //     <Item 
        //         title   = 'Cart' 
        //         iconName = { Platform.OS === 'android' ? 'md-cart' : 'ios-cart' }
        //         onPress = { () => { }}
        //     />
        // </HeaderButtons>
        // ),
    };
};

const styles = StyleSheet.create({
    screen : {
        flex : 1 
    },
    centered: {
        flex : 1, 
        justifyContent : 'center', 
        alignItems : 'center'
    }
});

export default ProductsOverviewScreen;