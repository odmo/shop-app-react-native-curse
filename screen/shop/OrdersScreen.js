import React, { useEffect, useState } from 'react';
import { StyleSheet, View, FlatList, Text, Platform, ActivityIndicator  } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';

import CustomHeaderButton from '../../components/UI/HeaderButton';
import OrderItem from '../../components/shop/OrderItem';
import Colors from '../../constant/Colors';
import * as ordersActions from '../../store/actions/orders'; 


const OrdersScreen = props => {

    const [ isLoading, SetIsLoading ] = useState( false );
    // se state."orders" <-- se tiene que llamar igual que en el reducer y el app 
    const orders = useSelector( state => state.orders );
    const dispatch = useDispatch();
    
    useEffect( () => {
        SetIsLoading( true );
        dispatch( ordersActions.fetchOrders() ).then( () => {
            SetIsLoading( false );
        } );

    }, [ dispatch ]);

    if ( isLoading ){
        return( 
            <View style = { styles.centered }>  
                <ActivityIndicator size = 'large' color = { Colors.primary } />
            </View>
        );
    }

    if ( orders.length === 0 ){
        return (
            <View style = { { flex : 1, justifyContent : 'center', alignItems : 'center' } } > 
                <Text> No orders found, maybe start ordering some products ? </Text>
            </View>
        );
    }

    return (
        <FlatList
            data = { orders }
            keyExtractor = { item => item.id }
            renderItem = { itemData => ( <OrderItem   
                amount = { itemData.item.totalAmount } 
                date = { itemData.item.readableDate }  
                items = { itemData.item.items }
                /> 
            )}
        />
    );
};

OrdersScreen.navigationOptions = {
    headerTitle : ' Your Orders ',
    headerLeft : () => (
        <CustomHeaderButton
            text = 'Menu'
            onSelect = { () => navData.navigation.toggleDrawer() }
            color = { (Platform.OS === 'android') ? Colors.accent : Colors.primary }
        />
    ) 
};

const styles = StyleSheet.create({
    centered : {
        flex : 1,
        justifyContent : 'center',
        alignItems : 'center'
    }
})

export default OrdersScreen;