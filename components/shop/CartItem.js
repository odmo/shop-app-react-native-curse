import React from 'react';
import { StyleSheet,View,Text, TouchableOpacity, TouchableNativeFeedback , Platform } from 'react-native';
import Colors from '../../constant/Colors';

const CartItem = props => {

    let TouchableCmp = ( ( Platform.OS === 'android' ) && ( Platform.Version >= 21 ) ) ? TouchableNativeFeedback : TouchableOpacity;

    return (
        <View style = { styles.CartItem } >
            <Text style = {styles.itemData} >
                <Text style = { styles.quantity } > { props.quantity } </Text> 
                <Text style = { styles.mainText } > { props.title } </Text>
            </Text>
            <View style = { styles.itemData } >
                <Text style = { styles.mainText } > ${ props.amount } </Text>
                <View styles = { styles.trash }>
                    {props.deletable && <TouchableCmp 
                        onPress = {  props.onRemove } 
                        styles = { styles.removeBotton }
                    >
                        <View style = { styles.trashButton } >
                            <Text> TRASH </Text>
                        </View>
                    </TouchableCmp>}
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    trashButton : {
        backgroundColor : Colors.primary,
        borderRadius : 3
    },
    trash : {
        backgroundColor : 'blue',
        borderRadius : 10,
        alignItems : 'center',
        overflow : 'hidden',
        justifyContent : 'space-around'
    },  
    CartItem : {
        padding: 10,
        backgroundColor : 'white',
        flexDirection : 'row',
        justifyContent : 'space-between',
        marginHorizontal : 20
    },
    itemData : {
        flexDirection : 'row',
        alignItems : 'center'
    },
    quantity : {
        color : '#888',
        fontSize : 16,
    },
    mainText : {
        fontSize : 16,


    },
    trash : {

    },
    removeBotton : {
        marginLeft : 20 
    }
});

export default CartItem;
