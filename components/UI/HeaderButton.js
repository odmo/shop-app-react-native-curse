import React from 'react';
//import { HeaderButton } from 'react-navigation-header-buttons';
import { StyleSheet, Button} from 'react-native';
//import { Icon } from 'react-native-vector-icons';
//import { Ionicons } from '@expo/vector-icons';
//import Colors from '../../constant/Colors';

const CustomHeaderButton = props => {

    return (
        <Button { ...props }
            style = { { ...styles.button, ...props.style } }
            title = { props.text }
            onPress={ props.onSelect }
            
        />
        // <HeaderButton 
        //     { ...props } 
        //     IconComponent = { Ionicons }
        //     iconSize = { 23 }
        //     color = { (Platform.OS === 'android') ? 'white' : Colors.primary }
        // />
    );
}; 

const styles = StyleSheet.create({
    button : {
        margin : 20,
        padding: 10,
    }
});

export default CustomHeaderButton;
